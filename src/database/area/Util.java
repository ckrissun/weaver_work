package database.area;

public class Util {
	
	public static boolean isEmpty(String str){
		if(str==null||str.equals("")){
			return true;
		}
		return false;
	}
	
	public static boolean likein(String str,String[] args){
		
		for(String arg : args){
			if(str.contains(arg)){
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean in(String str,String[] args){
		
		for(String arg : args){
			if(str.equals(arg)){
				return true;
			}
		}
		
		return false;
	}
	
	// 将字符串首字符转换成大写
	public static String getFirstUpper(String code){
		return code.substring(0,1).toUpperCase() + code.substring(1);
	}
	
	// 表名无前缀
	public static String getNoprefixAndLower(String code,String prix){
		return code.replace(prix, "");
	}
	
	public static String getKeyPrefix(String keyNo){
		return keyNo.substring(0,keyNo.lastIndexOf("_")).toLowerCase();
	}

}
