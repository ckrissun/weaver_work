package cc.ifelsefor.twoj.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import cc.ifelsefor.twoj.core.support.MyBufferedWriter;

/**
*
* <p>Title: Wang - RequestGet</p>
*
* <p> 请求JSP模板文件,获得新源代码文件
* <p>“<span style="color:red">”
* 
* <p>Copyright: WangHuarong's</p>
*
* <p>Company: personal</p>
*
* @author wanghr
* @version 1.0
*/
public class URLConnectGetTool {
	
	// 还原替换参数=>eg: new String[][]{{"${","$`{"},{"#{","#`{"},{"<%","<`%"},{"<jsp:","<`jsp:"},{"</jsp:", "<`/jsp:"}}
	String[][] reps;
	
	// 输出文件夹=>eg:C:\\Users\\Administrator\\workspace\\twoj\\new_src
	File newsrc_mod; 
	
	// web模板工程物理文件夹=>eg:C:\\Users\\Administrator\\Desktop\\demo1\\WebContent\\jsp
	File jsp_mod;
	
	// web模板工程父目录URL=>eg:http://localhost:8080/demo1/
	String jsp_modPath;
	
	public URLConnectGetTool(String jsp_modPath,String[][] reps,File newsrc_mod,File jsp_mod){
		this.jsp_modPath = jsp_modPath;
		this.reps = reps;
		this.newsrc_mod = newsrc_mod;
		this.jsp_mod = jsp_mod;
		if(!this.newsrc_mod.exists()){
			this.newsrc_mod.mkdirs();
		};
		if(!this.jsp_mod.exists()){
			this.jsp_mod.mkdirs();
		};
	}
	
	/**
	 * 从JSP服务器获得新的源代码
	 * @throws Exception 
	 * @throws Exception
	 */
	public void getSourceFromJsp() throws Exception{
		// JSP模块工程文件夹中文件集合
		List<File> jsp_modFiles = JspConverterTool.getAllFiles(jsp_mod);
		/* JSP模块工程文件夹中文件集合的网络请求,获得请求文件内容保存在本地新源代码输出文件夹中 */
		for(int i = 0; i< jsp_modFiles.size(); i++){
				String jsp = jsp_modFiles.get(i).getAbsolutePath();
				String dstPath = jsp.replace(jsp_mod.getAbsolutePath(),newsrc_mod.getAbsolutePath());
				dstPath = dstPath.substring(0,dstPath.length()-4);
		 		// 目标生成文件
		 		File rstFile = new File(dstPath);
		 		// 如果没有生成
		 		if(!rstFile.exists() && jsp_modFiles.get(i).isFile()){
		 			String url = jsp.replace(jsp_mod.getParentFile().getAbsolutePath(),"").substring(1).replace("\\","/");
		 			URL jspUrl = new URL(jsp_modPath+url);
		 			System.out.println(jspUrl.getPath());
		 			URLConnection conn = jspUrl.openConnection();
		 			String pageEncoding = conn.getContentType().split(";")[1].split("=")[1];
		 			System.out.println(pageEncoding);
		 			MyBufferedWriter mfw = null;
		 			BufferedReader ins = null;
		 			try{
		 				InputStream osm = jspUrl.openStream();
		 				mfw = new MyBufferedWriter(rstFile, pageEncoding, reps);
		 				ins = new BufferedReader(new InputStreamReader(osm, pageEncoding));
			 			String s = null;
			 			boolean first = true;
			 			boolean canStart = false;
			 			while((s = ins.readLine())!=null){
			 				if(canStart==false){
			 					if(s.matches(".*<!--.*headsplit.*-->.*")){
			 						canStart = true;
			 					}
			 				}else {
				 				if(first){
				 					mfw.write(s);
				 					first = false;
				 				}else{
				 					mfw.newLine();
				 					mfw.write(s);
				 				}
			 				}
			 			}
			 			mfw.flush();
		 			}catch(Exception e){
		 				e.printStackTrace();
		 			}finally{
		 				if(ins!=null){
		 					ins.close();
		 				}
		 				if(mfw!=null){
		 					mfw.close();
		 				}
		 			}
		 		}
		}
	}
	
	public static void main(String[] args) throws IOException {
		
	}
	
	
	
}
